$(".tabs p").click(function() {
	//find index of clicked tab
	var clicked_tab = $(this).index();
	//find index of previous active tab
	var previous_tab = $(".active-tab").index();
	//remove style of previous
	$(".active-tab").removeClass("active-tab");
	//add style to clicked tab
	$(this).addClass("active-tab");
	//show content of clicked tab
	$(".tab-text").eq(clicked_tab).css("display", "block");
	//hide content of previous tab
	$(".tab-text").eq(previous_tab).css("display", "none");
});


$(".slider-button").click(function() {
	//find index of clicked tab
	var clicked_button = $(this).index();
	//find index of previous active tab
	var previous_button = $(".active-slider").index();
	//remove style of previous
	$(".active-slider").removeClass("active-slider");
	//add style to clicked tab
	$(this).addClass("active-slider");
	//show content of clicked tab
	$("#slider-text h2").eq(clicked_button).css("top", 0);

	//hide content of previous tab
	$("#slider-text h2").eq(previous_button).css("top", "-100px");
	
});






$("#about-link").click(function(e) {
	e.preventDefault();
  $("html, body").animate({scrollTop: $('#about-section').offset().top}, 900);
});


$("#features-link").click(function(e) {
	e.preventDefault();
  $("html, body").animate({scrollTop: $('#phone-section').offset().top}, 900);
});

$("#contacts-link").click(function(e) {
	e.preventDefault();
  $("html, body").animate({scrollTop: $('#map-container').offset().top}, 900);
});
